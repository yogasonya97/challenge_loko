<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->string('id', 16)->primary();
            $table->string('name');
            $table->year('year');
            $table->string('author');
            $table->string('summary');
            $table->string('publisher');
            $table->integer('pageCount');
            $table->integer('readPage');
            $table->boolean('reading');
            $table->timestamp('insertedAt')->nullable();
            $table->timestamp('updatedAt')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
