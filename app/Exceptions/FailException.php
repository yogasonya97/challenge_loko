<?php

namespace App\Exceptions;

use Exception;

class FailException extends Exception
{
    public function render($request)
    {
        return response()->json(["status" => "fail", "message" => $this->getMessage()], 400);
    }
}
